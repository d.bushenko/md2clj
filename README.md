# md2clj

Translages CommonMark (Markdown) file into Clojure data structures.

## Building

    $ mvn package

## Usage from console

    $ java -jar target/md2clj-1.0-SNAPSHOT.jar input.md output.clj

## Usage from Clojure application

    (let [mdFile (slurp "input.md")] 
        (-> mdFile
            mbt.App/transform
            println))

### Example output
    
    [:document {}

    [:heading {:level 1}
        [:text {:literal "This is heading} ] ]
    
    [:paragraph {}
        [:text {:literal "This is paragraph."} ]
        [:soft-line-break {} ]
        [:text {:literal "Paragraph continues."} ] ]
