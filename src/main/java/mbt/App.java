package mbt;

import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.parser.Parser;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

// Commonmark specification: https://spec.commonmark.org/
//
public class App 
{
    // This method should be called from console
    public static void main( String[] args ) throws IOException {
        if (args.length < 2) {
            System.out.println("Error!\nThe application accepts two arguments: input and output file names!");
            System.exit(-1);
        }

        final var extensions = Arrays.asList( TablesExtension.create() );
        final var parser = Parser.builder().extensions(extensions).build();
        final var path = Paths.get(args[0]);
        final var mdfile = Files.readString(path, Charset.defaultCharset());
        final var rootNode = parser.parse(mdfile);
        final var visitor = new ParseMdVisitor();

        rootNode.accept(visitor);

        visitor.writeOutput(args[1]);

        System.out.println( "Processed" );
    }

    // This method is for calling from another Clojure app
    public static String transform(final String input) {
        final var extensions = Arrays.asList( TablesExtension.create() );
        final var parser = Parser.builder().extensions(extensions).build();
        final var rootNode = parser.parse(input);
        final var visitor = new ParseMdVisitor();

        rootNode.accept(visitor);

        return visitor.getTransformed();
    }
}
