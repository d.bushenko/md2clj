package mbt;

import org.commonmark.ext.gfm.tables.*;
import org.commonmark.node.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class ParseMdVisitor extends AbstractVisitor {
    private final StringBuilder acc = new StringBuilder();

    // Returns EDN string from parsed Markdown
    public String getTransformed() {
        return acc.toString();
    }

    @Override
    public void visit(BlockQuote blockQuote) {
        acc.append("[:block-quote {} ");
        visitChildren(blockQuote);
        acc.append("] ");
    }

    @Override
    public void visit(BulletList bulletList) {
        acc.append("[:bullet-list ");
        acc.append("{:bullet-marker \"" + bulletList.getBulletMarker() + "\", ");
        acc.append(":tight? " + bulletList.isTight() + "} ");
        visitChildren(bulletList);
        acc.append("] ");
    }

    @Override
    public void visit(Code code) {
        acc.append("[:code ");
        acc.append("{:literal \"" + code.getLiteral() + "\"} ");
        visitChildren(code);
        acc.append("] ");
    }

    @Override
    public void visit(Document document) {
        acc.append("[:document {} ");
        visitChildren(document);
        acc.append("] ");
    }

    @Override
    public void visit(Emphasis emphasis) {
        acc.append("[:emphasis ");
        acc.append("{:opening-delimiter \"" + emphasis.getOpeningDelimiter() + "\", ");
        acc.append(":closing-delimiter \"" + emphasis.getClosingDelimiter() + "\"}");
        visitChildren(emphasis);
        acc.append("] ");
    }

    @Override
    public void visit(FencedCodeBlock fencedCodeBlock) {
        acc.append("[:fenced-code-block ");
        acc.append("{:fence-char \"" + fencedCodeBlock.getFenceChar() + "\", ");
        acc.append(":fence-length " + fencedCodeBlock.getFenceLength() + ", ");
        acc.append(":fence-indent " + fencedCodeBlock.getFenceIndent() + ", ");
        acc.append(":info \"" + fencedCodeBlock.getInfo() + "\", ");
        acc.append(":literal \"" + fencedCodeBlock.getLiteral() + "\"} ");

        visitChildren(fencedCodeBlock);
        acc.append("] ");
    }

    @Override
    public void visit(HardLineBreak hardLineBreak) {
        acc.append("[:hard-line-break {} ");
        visitChildren(hardLineBreak);
        acc.append("] ");
    }

    @Override
    public void visit(Heading heading) {
        acc.append("[:heading ");
        acc.append("{:level " + heading.getLevel() + "} ");
        visitChildren(heading);
        acc.append("] ");
    }

    @Override
    public void visit(ThematicBreak thematicBreak) {
        acc.append("[:thematic-break {} ");
        visitChildren(thematicBreak);
        acc.append("] ");
    }

    @Override
    public void visit(HtmlInline htmlInline) {
        acc.append("[:html-inline ");
        acc.append("{:literal \"" + htmlInline.getLiteral() + "\"} ");
        visitChildren(htmlInline);
        acc.append("] ");
    }

    @Override
    public void visit(HtmlBlock htmlBlock) {
        acc.append("[:html-block ");
        acc.append("{:literal \"" + htmlBlock.getLiteral() + "\"} ");
        visitChildren(htmlBlock);
        acc.append("] ");
    }

    @Override
    public void visit(Image image) {
        acc.append("[:image ");
        acc.append("{:title " + "\"" + image.getTitle() + "\", ");
        acc.append(":destination \"" + image.getDestination() + "\"} ");
        visitChildren(image);
        acc.append("] ");
    }

    @Override
    public void visit(IndentedCodeBlock indentedCodeBlock) {
        acc.append("[:indented-code-block ");
        acc.append("{:literal \"" + indentedCodeBlock.getLiteral() + "\"} ");
        visitChildren(indentedCodeBlock);
        acc.append("] ");
    }

    @Override
    public void visit(Link link) {
        acc.append("[:link ");
        acc.append("{:title " + "\"" + link.getTitle() + "\", ");
        acc.append(":destination \"" + link.getDestination() + "\"} ");
        visitChildren(link);
        acc.append("] ");
    }

    @Override
    public void visit(ListItem listItem) {
        acc.append("[:list-item {} ");
        visitChildren(listItem);
        acc.append("] ");
    }

    @Override
    public void visit(OrderedList orderedList) {
        acc.append("[:ordered-list ");
        acc.append("{:start-number " + orderedList.getStartNumber() + ", ");
        acc.append(":delimiter " + orderedList.getDelimiter() + ", ");
        acc.append(":tight? " + orderedList.isTight() + "} ");
        visitChildren(orderedList);
        acc.append("] ");
    }

    @Override
    public void visit(Paragraph paragraph) {
        acc.append("[:paragraph {} ");
        visitChildren(paragraph);
        acc.append("] ");
    }

    @Override
    public void visit(SoftLineBreak softLineBreak) {
        acc.append("[:soft-line-break {} ");
        visitChildren(softLineBreak);
        acc.append("] ");
    }

    @Override
    public void visit(StrongEmphasis strongEmphasis) {
        acc.append("[:strong-emphasis ");
        acc.append("{:opening-delimiter \"" + strongEmphasis.getOpeningDelimiter() + "\", ");
        acc.append(":closing-delimiter \"" + strongEmphasis.getClosingDelimiter() + "\"}");
        visitChildren(strongEmphasis);
        acc.append("] ");
    }

    @Override
    public void visit(Text text) {
        acc.append("[:text ");
        acc.append("{:literal \"" + text.getLiteral() + "\"} ");
        visitChildren(text);
        acc.append("] ");
    }

    @Override
    public void visit(LinkReferenceDefinition linkReferenceDefinition) {
        acc.append("[:link-reference-definition ");
        acc.append("{:label " + "\"" + linkReferenceDefinition.getLabel() + "\", ");
        acc.append(":title " + "\"" + linkReferenceDefinition.getTitle() + "\", ");
        acc.append(":destination \"" + linkReferenceDefinition.getDestination() + "\"} ");
        visitChildren(linkReferenceDefinition);
        acc.append("] ");
    }

    @Override
    public void visit(CustomBlock customBlock) {
        if (customBlock instanceof TableBlock) {
            visit((TableBlock)customBlock);

        } else {
            acc.append("[:custom-block {} ");
            visitChildren(customBlock);
            acc.append("] ");
        }
    }

    public void visit(TableBlock tableBlock) {
        acc.append("[:table-block {} ");
        visitChildren(tableBlock);
        acc.append("] ");
    }

    @Override
    public void visit(CustomNode customNode) {
        if (customNode instanceof TableHead) {
            visit((TableHead)customNode);

        } else if (customNode instanceof TableBody) {
            visit((TableBody)customNode);

        } else if (customNode instanceof TableRow) {
            visit((TableRow)customNode);

        } else if (customNode instanceof TableCell) {
            visit((TableCell)customNode);

        }else {
            acc.append("[:custom-node {} ");
            visitChildren(customNode);
            acc.append("] ");
        }
    }

    public void visit(TableHead tableHead) {
        acc.append("[:table-head {} ");
        visitChildren(tableHead);
        acc.append("] ");
    }

    public void visit(TableBody tableBody) {
        acc.append("[:table-body {} ");
        visitChildren(tableBody);
        acc.append("] ");
    }

    public void visit(TableRow tableRow) {
        acc.append("[:table-row {} ");
        visitChildren(tableRow);
        acc.append("] ");
    }

    public void visit(TableCell tableCell) {
        acc.append("[:table-cell {} ");
        visitChildren(tableCell);
        acc.append("] ");
    }


    public void writeOutput(final String outputPath) throws IOException {
        final var path = Paths.get(outputPath);
        if (Files.exists(path)) {
            Files.delete(path);
        }

        Files.writeString(path, acc.toString(), Charset.defaultCharset(), StandardOpenOption.CREATE_NEW);
    }
}
